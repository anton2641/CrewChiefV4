# Game-Specific Command Line Switches

Use the following command line switches to configure the Crew Chief for specific games:

## Specify Game

`-game GAME_NAME` - Select a game in the Crew Chief  
Example: `[full path]\CrewChiefV4.exe -game RACE_ROOM`  
This selects RaceRoom as the game in the Crew Chief.

**Supported values:**

- ACC *(Assetto Corsa Competizione)*
- AMS *(Automobilista)*
- AMS2, AMS2_NETWORK
- ASR *(ARCA Sim Racing)*
- ASSETTO_64BIT, ASSETTO_32BIT
- F1_2018, F1_2019, F1_2020, F1_2021
- FTRUCK
- GSC *(Game Stock Car)*
- IRACING
- MARCAS
- PCARS2, PCARS2_NETWORK
- PCARS_64BIT, PCARS_32BIT, PCARS_NETWORK
- RACE_ROOM
- RBR *(Richard Burns Rally)*
- RF1
- RF2 *(Note: 32-bit rFactor 2 is no longer supported, and RF2_64BIT is now not recognized)*

Combine this switch with Crew Chief's `launch_[game] / [game]_launch_exe / [game]_launch_params`, `run_immediately`, and `-game` options to configure Crew Chief to start the specified game and its own process.

## Specify Profile

`-profile profile file name` - Specify the profile to run at Crew Chief startup  
Example: `[full path]\CrewChiefV4.exe -profile "my favourite game my awesome profile"`  
This loads the `"my favourite game my awesome profile.json"` profile when Crew Chief starts.

## Set Processor Affinity

`-cpu[1-8]` - Set the processor affinity for Crew Chief in Task Manager, which must be done each time you start Crew Chief. Alternatively, start Crew Chief with an additional argument "-cpu1", "-cpu2", ... "-cpu8":  
Example: `[full path]\CrewChiefV4.exe -cpu4`  
This sets the processor affinity to the 4th CPU in your system (usually referred to as CPU3 - they're zero-indexed).

## Additional Switches

- `-c_exit` - Close the running Crew Chief instance.
- `-nodevicescan` - Disable automatic active/disabled controller detection. Use this if you have issues with Crew Chief rescanning controllers all the time (caused by buggy device drivers).
- `-sound_test` - Enable extra UI that helps sound pack creators test sounds.
- `-skip_updates` - Disable the check for Crew Chief updates.
- `-debug` - Collect Crew Chief debug trace. For more info, see [here](https://thecrewchief.org/showthread.php?142-How-to-collect-Crew-Chief-repro-traces).
