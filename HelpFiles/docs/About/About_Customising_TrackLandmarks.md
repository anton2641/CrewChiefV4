# Recording Track Landmarks (Corner Mappings)

To ensure optimal results, please keep corner mappings tight (from turn-in point to corner exit) and exclude straights.

## Steps to Record Track Landmarks

1. **Assign buttons**: Assign two buttons - one for starting/stopping the recording ("Start / stop track landmarks recording" action) and one for adding start/end points ("Add Start / end track landmark" action).

2. **Start a practice session**: Wait until you're on the track and press the "Start / stop track landmarks recording" button.

3. **Add landmarks**: Begin adding landmarks by pressing the "Add Start / end track landmark" button. The first press marks the start point of the track landmark, and the second press marks the end point.

4. **End the recording**: Press the "Start / stop track landmarks recording" button to finish recording. This will create a file named `trackLandmarks.json` in `C:\Users\<user_name>\Documents\CrewChiefV4\track_landmarks\<game>\<track>\`.

## Track Landmarks (Corner Mappings) Components

- Track Name or Raceroom ID
- List of:
  - landmarkName
  - distanceRoundLapStart
  - distanceRoundLapEnd
  - isCommonOvertakingSpot (default false)

The file will have default corner names ("turn 1", "turn 2", etc.) with `isCommonOvertakingSpot` set to false. Edit the file to change `isCommonOvertakingSpot` to 'true' where appropriate, and modify corner names if needed.

To use custom corner names in Crew Chief, they must be in `C:\Users\<user_name>\AppData\local\CrewChiefV4\sounds\voice\corner_names\`.

Request new names on the Crew Chief forum if necessary.

## Share Your Recordings 🎉

Share your recordings with the community by uploading them to the Crew Chief forum, and they will be included in the next update.

**Important**: Restart Crew Chief to load your newly added landmarks file!
