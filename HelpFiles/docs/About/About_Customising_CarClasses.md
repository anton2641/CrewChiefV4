# Customizing Car Classes in Crew Chief

## Why Customize Car Classes?

Customizing car classes in Crew Chief can enhance your experience, especially when using mods that Crew Chief doesn't support out of the box. Crew Chief relies on class definitions for information about wheel sizes, brake types, etc. This mechanism helps ensure compatibility with a wide range of third-party content.

## Single-Class vs Multi-Class Events

Crew Chief uses Car Class to distinguish between single-class and multi-class races. In multi-class races, Crew Chief compares your lap times with those of vehicles in the same class. However, some mods may have different car class names for vehicles in the same class, causing confusion. For instance, "A GT3" class name for brand A and "B GT3" for brand B. To have Crew Chief treat both as the same class, map "A GT3" and "B GT3" class names to the Crew Chief GT3 class, turning the race into a single-class event.

## What Can Be Customized?

1. **Map a vehicle to a pre-defined Crew Chief class**: For example, if your vehicle class is "Super Mod GT3", you can map it to the GT3 class.
2. **Customize pre-defined Crew Chief car classes**: For instance, if you're driving a vehicle with large wheels, Crew Chief might announce frequent wheel locking (due to slower wheel rotation than expected). The Car Class configuration mechanism allows you to specify wheel sizes.

For more details, check out this forum post on [Customizing Car Classes](https://thecrewchief.org/showthread.php?142-Customising-car-classes).