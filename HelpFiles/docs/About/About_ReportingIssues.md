# Reporting Issues

Crew Chief continuously logs various types of information in its "Console window." When Crew Chief exits, it saves all the data in a file located in the folder `<Documents>\CrewChiefV4\debugLogs`

(e.g., c:\users\Documents<your name>\CrewChiefV4\debugLogs).

Each file has a timestamp in the name, indicating the date and time it was saved (e.g., c:\users\Documents<your name>\CrewChiefV4\debugLogs\console_2022_03_23-20-12-26.txt, formatted as year_month_day-hour-minute-second). Crew Chief retains the 25 most recent files and deletes older ones.

You can also save a log file at any time by right-clicking on the Console window.

## 🛠️ Troubleshooting with Log Files 🛠️

If Crew Chief isn't working properly, it's crucial to upload the log file saved after encountering the issue to the forum, Discord, or the confidential email address.

⚠️**Note**: If Crew Chief crashes, the log file won't be saved, but a previous one may still be helpful.
