# 🌟 Crew Chief Credits

Crew Chief is the result of the hard work and dedication of many talented individuals. We'd like to give a special shout-out to everyone who contributed to its creation, as well as the advice and support from the community, Sector3, and SMS.

## 💡 The Main Team 💡

- Jim Britton
- Morten Roslev
- Vytautas Leonavičius
- Paul Burgess
- Tony Whitley
- Dan Allongo (Automobilista and rFactor1 implementation)
- Daniel Nowak (nAudio speech recognition port)
- Mike Schreiner (technical input on stock car rules)
- Brent Owen (technical input on stock car rules)

## 🎨 Additional Contributions 🎨

- Scoops (track layout mapping work)
- Nolan Bates (conversion of thousands of phrases for subtitle support)
- Longtomjr (F1 2018 UDP data format structs)

## 🎙️ Alternate Spotter Sounds 🎙️

- Geoffrey Lessel
- Matt Orr (aka EmptyBox)
- Clare Britton
- Mike Schreiner
- Phil Linden
- Lee Taylor
- Micha (last name?)

## 📚 Help Files 📚

- Tony Whitley (help files created from the original text content)

## 🚗 Rally Mode 🚗

- Janne Laahanen (contributed co-driver pack and helped in understanding RBR pacenotes)

## 🏁 Special Thanks 🏁

- Nick Thissen (for his work on iRacingSdkWrapper)

Thank you to everyone involved in making Crew Chief a reality! 🏆