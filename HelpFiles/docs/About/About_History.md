In the words of Jim Britton (aka Mr Belowski, the creator as well as the
voice of Crew Chief) *"Back in 2015 I was playing Raceroom a lot and you
had to decide between switching on these massive ugly overlays or having
no fuel & penalty information. While on holiday I was sitting in a sauna
thinking about it and I decided to address those specific issues with a
little radio app which called out penalties and a couple of other
things. Really narrow scope initially but it grew quickly and the
Raceroom community liked the idea. Suggestions for improvements started
coming in and it grew from there.  
  
At the time, the goal was to build something as good as the iRacing
spotter / chief (which was and still is very good) so I had a lot of
things to add. There was a V1, it was rubbish. I made it less rubbish by
adding a crude UI and other things, calling it V2. This was late 2015.
Then I added voice recognition and some other things in 2016 and called
it V3 'cos it was much better. Then I made the auto updater and added a
load of other stuff mid 2017 and called it V4. Then I realised that the
auto updater code meant that changing it to V5 would create a lot of
additional boring work that I didn't want to do, so it's been stuck at
V4 ever since.  
  
In late 2015 Project Cars was the first game added, it required a major
overhaul to decouple the game from Crew Chief's internal state.  Other
games were added later, more easily now that Crew Chief could handle
them more like plug-ins.  
  
If it had just been me working on it, there's no doubt it would have
stayed rubbish. I'm incredibly grateful to the guys who've worked with
me\* on it* (See About/Credits) *and to the community for their ideas,
support and encouragement. But 7 years... still boggles the mind!"  
  
\** *now referred to tongue in cheek as **Crew Chief Mega Corp***

#### Selected highlights

2015 V1, 2 and 3 for Raceroom. Driver names. Project Cars added.  
2016: V4. Voice commands. Assetto Corsa, rFactor and Automobilista
added.  
2017: iRacing and Project Cars 2 added. Pit command macros for Raceroom
and iRacing.  
2018: rFactor 2 and F1 2018 (spotter only) added.  
2019: F1 2019 (spotter only) and Assetto Corsa Competizione added.  
2020: F1 2020 (spotter only), Automobilista 2, Richard Burns Rally and
Dirt Rally 1&2 added.  Pit commands for rFactor 2.  
2021: GTR2 added. Pit commands for Assetto Corsa Competizione.  
  
  
