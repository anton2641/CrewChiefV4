# Telemetry

CrewChief can publish telemetry data to an MQTT broker. It also
subscribes to an MQTT topic and will trigger notifications based on
messages on that topic. The general idea is to enable live online
telemetry analysis.  
Enable the telemetry collection by checking 'MQTT Telemetry enabled' and
choose a 'MQTT drivername' in the CrewChief options.  
A community operated broker is available at
[github.com/b4mad/racing](https://github.com/b4mad/racing) and you can
access your telemetry at
[pitwall.b4mad.racing](https://pitwall.b4mad.racing/)  
Detailed settings are available `My
Documents\CrewChiefV4\mqtt_telemetry.json`
