# Personalized Names in Crew Chief

## Real Voice Recordings, Not Text-To-Speech 🗣️

Crew Chief names are not robotic text-to-speech; they have been individually recorded by Jim. Each of the names has 24 unique messages (including some sweary ones), so when you hear Jim swearing at you, it's *personal*. This has been a significant undertaking, with 750 names (each with 24 recordings) available.

## Finding Your Name in Crew Chief 🔍

1. **Check the existing list**: First, [verify](https://thecrewchief.org/showthread.php?351-Full-list-of-all-My-Name-entries-(-quot-Personalisations-quot-)) if your name or something similar is already in Crew Chief.
2. **Consider a "stub" version**: If your name isn't available, see if one of the nearly 8000 ["stub" versions](https://thecrewchief.org/showthread.php?1705-Personalisations-an-experiment) works for you.
3. **Read Jim's note**: If neither of the above options works, read [Jim's note](https://thecrewchief.org/showthread.php?491-New-personalisation-requests) on personalization requests.

## Requesting a New Name 📝

If you still can't find a suitable name, you can add your name request to the queue in the [Crew Chief Forum](https://thecrewchief.org/forumdisplay.php?7-Names-and-personalisations).