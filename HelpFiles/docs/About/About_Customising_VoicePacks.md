# Customising Voice Packs

## I need a sound pack for my language, can I record it?  
A: Crew Chief is an open source project, we can't stop you from creating
a sound pack for it if you would like to do that. However, it is a large
commitment. Before beginning, it is worth checking
`c:\Users[your_login_name]\AppData\Local\CrewChiefV4\sounds` for the
scale of work involved, to see if you can commit to the work. Every
sound in Crew Chief is human recorded and edited by Jim Britton
(mr_belowski). Also, keep in mind that application is evolving, so the
pack will need maintenance.  
  
Depending on how different sentence/number formatting is in your
language, it might require code changes to the Crew Chief code.
Depending on how much work it is we may or may not be able to help.  
  
More in this forum post [Authoring alternative Crew Chief voice
packs](https://thecrewchief.org/showthread.php?825-Authoring-alternative-Crew-Chief-voice-packs)  
