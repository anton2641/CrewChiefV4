# Updating Crew Chief

If a new version of Crew Chief is available the auto updater will prompt
you to download it. This will download and run a new .msi installer -
just point it at the existing install location and it'll update your old
installation. It won't remove your existing sound pack or your
settings.  

## Updating the sound pack

If a new sound pack or driver names pack is available the appropriate
Download button(s) will be enabled - these will download and unpack the
updated sounds / driver names, then restart Crew Chief.  

## Updating the speech recognition engine

The 64bit speech recognition installers can be downloaded
[here](https://drive.google.com/file/d/0B4KQS820QNFbY05tVnhiNVFnYkU/view?usp=sharing)  
The 32bit speech recognition installers can be downloaded
[here](https://drive.google.com/file/d/0B4KQS820QNFbRVJrVjU4X1NxSEU/view?usp=sharing)  
  