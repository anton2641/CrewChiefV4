# Known Issues and Limitations

There are some known issues that cannot be fixed:

## RaceRoom

- **Driver identification:** RaceRoom's 'slot_id' field for uniquely identifying drivers in a race has many issues, so Crew Chief uses driver names instead. Driver names for AI drivers are not unique, and if a race has two or more drivers with the same name, Crew Chief will get lap and sector times wrong. This is only a problem when racing against AI drivers.
- **Pre-start procedure phase:** RaceRoom does not have a pre-start procedure phase for offline races. In the pre-start phase online ("Gridwalk"), very little valid and accurate data is available.

## Project Cars

- **Opponent lap time data:** Project Cars does not send opponent lap time data, so Crew Chief has to time their laps. In practice and qualifying sessions, this is fairly reliable, as Crew Chief can use the session's remaining time for its clock. However, in race sessions with a fixed number of laps, there is no clock to time the laps, which can lead to inaccuracies in opponent lap/sector times if the player pauses the game.
- **Joining a session partway through:** Joining a practice or qualifying session online partway through results in Crew Chief having an incomplete set of data for opponent lap and sector times. The best opponent lap and sector data may be inaccurate, as it relies on the fastest lap completed while Crew Chief is running.
- **Opponent car class data:** Project Cars does not send opponent car class data, so Crew Chief has to assume that all drivers in the race are in the same car class. For multi-class races, all pace and other reports will be relative to the overall leader/fastest car.
- **Pre-start procedure phase:** Project Cars does not have a distinct pre-start procedure phase. Additional messages have been added before the "get ready" message, but they might delay the "get ready" message.
  
## Detecting 'good' passes

Detecting 'good' passes is not feasible. Crew Chief tries to limit the 'good pass' messages to overtakes that are reasonably secure, don't result in the other car slowing excessively, and don't involve the player going off-track. However, it cannot distinguish between a clean pass and a bump-and-run punt, so you might get congratulated for driving aggressively.
