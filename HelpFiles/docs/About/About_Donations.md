# Donations

We created and maintain Crew Chief because we're passionate about making stuff and contributing to the sim racing community. Working with various quirks, errors, and omissions in the shared data provided by the games can be challenging, but it's all part of the experience. Countless hours of hard work have been invested in this project.

## 💖 Show Your Support 💖

If you enjoy using Crew Chief and it has become a regular, positive part of your sim racing experience, please consider making a small donation. Even if it's just to stop our spouses from complaining!

You can donate to Crew Chief's PayPal address at jim.britton@yahoo.co.uk or use this link to donate directly

[ :fontawesome-brands-paypal: Donate to Crew Chief](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LW33XFXP4DPZE){ .md-button .md-button--primary }

While recouping some of the investment would be great, our primary goal is for Crew Chief to be used 'in anger' and enjoyed as part of the sim racing experience. We're always on the lookout for bug reports and feature suggestions.

## One Last Thing

If Crew Chief ever says, *"Jim is faster than you"*, be sure to let him through! 😎