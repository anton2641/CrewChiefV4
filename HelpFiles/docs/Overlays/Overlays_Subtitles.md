# Subtitle Overlays

Crew Chief can display subtitle overlay of what the chief, spotter and
you say.  This only works with the game running in windowed (or
borderless windowed) mode.  
  
To enable this use the setting "Enable Subtitle overlay" in the
Properties.  Once enabled you should see a subtitle bar at the top of
your primary screen.  
By default the subtitle window will be shown with settings to adjust it,
before the window will accept input you have to click the checkbox
"Enable input" on its titlebar, once enabled you can move the overlay
window to your desired location and change its settings which are pretty
much self-explanatory.  
  
Dont forget to save settings once done.  
  
To show/hide the settings/titlebar press CTRL + SHIFT.  
  
For Oculus users the overlay can be rendered as a separate application
window, allowing it to be added to the VR world - enable the 'Enable
overlay app window (Oculus mode)' checkbox in the Properties screen.  
