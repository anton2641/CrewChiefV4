# In-Game Overlays and Telemetry Charts in Crew Chief

Crew Chief can render in-game overlays to display console output or user-configurable telemetry charts controlled by voice commands. This functionality is available in windowed (or borderless windowed) mode and can be shown in VR using third-party apps like "OVR Overlay".

## Enabling Overlays and Telemetry

Enable in-game overlays by checking the 'Enable overlay window' property. Telemetry recording is available for practice sessions but not during races. Enable race session telemetry by checking the 'Enable chart telemetry in race session' property.

For iRacing, enable disk telemetry in iRacing app.ini 'irsdkEnableDisk' and in Crew Chief properties 'Enable disk based telemetry for overlay'.

## Accessing Telemetry Data

Telemetry data is available for:

- The previous completed lap (excluding out laps)
- The player's best lap in the session (excluding invalid or incomplete laps)
- The best opponent lap in the session (limited to car speed and only available for some games)

Typically, you would drive a few laps in practice, pit, and then ask the Chief "show me last lap car speed" or similar.

## Overlay Controls

Control the overlays and charts with voice commands or by enabling the "Enable input" checkbox on the overlay to display mouse controls.

### General overlay commands

- "hide overlay" / "close overlay""
- "show overlay"
- "show console"
- "hide console"
- "show chart"
- "hide chart"
- "show all overlays" - show console and chart(s)
- "new chart" / "clear chart" - removes all data from the currently rendered chart
- "clear data" - clears all in-memory telemetry data for the current session
- "refresh chart" - redraw the current chart with the latest data
- "show stacked charts" - show each different series on its own chart
- "show single chart" - show all active series on the same chart
- "show time" - change the x-axis to be time
- "show distance" - change the x-axis to be distance around the lap (default)
- "Show sector [one / two / three]"
- "Show all sectors"
- "Zoom [in / out]"
- "Pan [left / right]"
- "Reset zoom" - reset zoom to show the entire lap's data
- "Next lap" - show the next lap (when showing 'last lap' chart)
- "Previous lap" - show the previous lap (when showing 'last lap' chart)
- "Show last lap" - move back to the last lap (when showing 'last lap' chart)

### Series specific overlay commands

- "show me..." - add a series to the chart
- "chart, remove..." - remove a series from the chart
- "best lap..." - add player best lap to the chart
- "last lap..." - add player last lap to the chart (can be omitted - Crew Chief will assume you mean last lap if you don't specify it)
- "opponent best lap.." - add opponent best lap (over all opponents in the player's class) to the chart

#### Examples

1. Single chart for car speed, best lap and opponent's overall best lap with x-axis as distance (meters):

```
"show me best lap car speed"
"show me opponent best lap car speed"
```

2. Two charts: one for speed and one for gear, best lap and last lap with x-axis as distance (meters):

```
"show me best lap car speed"
"show me last lap car speed"
"show me best lap gear"
"show me last lap gear"
```

3. Three charts: speed, gear, and RPM for best and last laps with x-axis as time (seconds):

```
"show me best lap car speed"
"show me last lap car speed"
"show me best lap gear"
"show me last lap gear"
"show me best lap engine revs"
"show me last lap engine revs"
"show time"
```

4. Single chart with throttle position and gear for the last lap with x-axis as distance (meters):

```
"show me throttle position"
"show me gear"
"show single chart"
```

**Note:** Data for the same series (e.g., car speed) will always be overlaid on the same chart. Stacked charts only apply to data from different series (speed / gear, for example).

## Series Definition and Voice Commands

Series definitions are held in `Documents/CrewChiefV4/chart_subscriptions.json`. You can add more series, but opponent data is limited. Voice commands for each series are also in this JSON file. The voice command is constructed by Crew Chief, prefixing "show me best lap...", "show me last lap...", or "show me opponent best lap..." as appropriate (e.g., "show me last lap car speed").

## Overlay Control with Mouse

Enable the "Enable input" checkbox on the overlay to show controls for various chart and overlay functions. The overlay can also be moved around by dragging the title bar when "Enable input" is checked.

## Overlay in VR for Oculus Users

For Oculus users, the overlay can be rendered as a separate application window, allowing it to be added to the VR world. Enable the 'Enable overlay app window (Oculus mode)' checkbox in the Properties screen.
