# rFactor 1/Automobilista/Game Stock Car Extreme/Formula Truck

Copa Petrobras de Marcas / ARCA Sim Racing:  
  
You need Dan Allongo's plugin for these games. Crew Chief will check if
this plugin is installed in the correct game folder. If it isn't, Crew
Chief will offer to install it for you when you start Crew Chief with
the game selected. In most cases all you need do is click the **OK**
button in the popup.  
  
If this process fails for any reason, copy the
rFactorSharedMemoryMap.dll from Crew Chief's plugins folder - usually
C:\Program Files (x86)\Britton IT
Ltd\CrewChiefV4\plugins\rfactor1\Plugins\rFactorSharedMemoryMap.dll to
the the game's 'Plugins' folder (usually something like C:\Program Files
(x86)\Steam\steamapps\common\Automobilista\Plugins). Once you've done
this just select the game from Crew Chief's Game menu and press the
Start button.  
  
The source code for this plugin is
[here](https://github.com/dallongo/rFactorSharedMemoryMap).  
  