# Assetto Corsa

You need Morten Roslev's Python plugin for Assetto Corsa. Crew Chief
will check if this plugin is installed in the correct game folder. If it
isn't, Crew Chief will offer to install it for you when you start Crew
Chief with Assetto Corsa selected. In most cases all you need do is
click the **OK** button in the popup.  
  
If this process fails for any reason, copy the folder called
`CrewChiefEx` from Crew Chief's plugins folder - usually `C:\ProgramFiles (x86)\Britton IT
Ltd\CrewChiefV4\plugins\assettocorsa\apps\python\CrewChiefEx` to the
Asstto Corsa python folder (usually something like `C:\Program Files
(x86)\Steam\steamapps\common\assettocorsa\apps\python`). You might need
to enable this plugin in the Assetto Corsa UI. Once you've done this
just select the game from Crew Chief's Game menu and press the Start
button.  
  