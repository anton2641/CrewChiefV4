# Using controller buttons

You can bind buttons on your controller(s) to some Crew Chief actions.
A good example is binding a button to "**Talk to Crew Chief**" (though
you can also alert Chief in other ways)  
  
![Control Panel](../assets/CC_Talk_to_CC.png)
