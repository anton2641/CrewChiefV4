# nAudio

## Choosing Audio methods

Crew Chief offers the choice of using **nAudio** instead of the standard
Windows audio library.  There are two Properties for selecting the audio
method:  

### Use nAudio for playback

Recommended. Using nAudio for playback allows device selection and
higher volumes. It can also play messages in stereo.  
*However,* nAudio has been known to cause some issues so it's worth
disabling it if Crew Chief is misbehaving.  
  
nAudio offers two output interface types - **WaveOut** and **WASAPI**.
WaveOut is more compatible, but WASAPI is more responsive/direct
playback.  
  
If not using nAudio for playback you might need to change your default
sound 'playback' device in the Windows Control Panel so Crew Chief sends
audio to the right device. Windows 10 also includes the option to route
audio from a specific application to a chosen output device.  

### Use nAudio for speech recognition

Not recommended. It allows speech recognition device selection but voice
recognition **Trigger word** and **Press and release button** are not
available with nAudio. Also Crew Chief works better if this setting is
disabled and instead your preferred microphone is set as the default
sound recording device in the Windows sounds Control Panel.  
