# GTR2

See this post on the forum:  
[Link](https://thecrewchief.org/showthread.php?2012-GTR2-Setup-Instructions-and-Known-Issues)
  
If you have a DVD version of GTR 2 you *may* be able to use the CD key
to register the Steam version.  
[Steam Support - Retail CD
Keys](https://support.steampowered.com/kb_article.php?s=3593ea91d4f906893707c13113c63594&ref=7480-WUSF-3601)  
