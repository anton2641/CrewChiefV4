# General Information

## Installation

Crew Chief requires the .Net framework version 4.7.2. While included in Windows 10 & 11, Windows 7 and Windows 8 users must install it separately.

- Download the CrewChiefV4.msi installer [from the forum](https://thecrewchief.org/forum.php) and run it.
- Launch Crew Chief
- Click the **Download sound pack** button and the **Download driver names** button to obtain the latest sounds and driver names.
- Select a game from the top-right list.
- Once the sounds and driver names finish downloading, click the **Start Application** button
- Start the game.

## Swearing and Complaints

By default, Crew Chief has swearing disabled. To enable swearing, adjust the setting in the Properties UI.

To limit the number of complaining or negative messages, use the **Max complaints per session** Property. Adjust this to a higher number if you don't mind the complaints, or set it to 0 to eliminate complaints. The default value of 60 helps prevent excessive grumbling during endurance or very long races.

## Game Configuration

Crew Chief is self-contained and requires minimal configuration for Raceroom, iRacing, and Project Cars. For rFactor 1-based games, rFactor 2, and Assetto Corsa, additional steps are required (see *Getting Started/Game specific*).

## Voice Recognition

For optional voice recognition, consult [Voice Recognition/Installation & Training](../Voice_Recognition/VoiceRecognition_InstallationTraining.md).

## Troubleshooting Startup Issues

If Crew Chief fails to start or starts and then closes, try deleting its configuration files. Navigate to `C:\Users\[user name]\AppData\Local\Britton_IT_Ltd\` and delete all subfolders, resetting Crew Chief settings to default. Note that the 'AppData' folder is hidden by default on Windows. To reveal it, adjust the File Explorer Options in Windows Control Panel, looking for a setting called **Show hidden files and folders**.

In case of issues, examine the Windows Event logs for .Net errors in the Crew Chief Logs section.
