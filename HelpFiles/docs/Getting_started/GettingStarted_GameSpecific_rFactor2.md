# rFactor 2

You need Vytautas Leonavičius' plugin for rFactor 2. Crew Chief will
check if this plugin is installed in the correct game folder. If it
isn't, Crew Chief will offer to install it for you when you start Crew
Chief with rF2 selected. In most cases all you need do is click the "OK"
button in the popup.  
  
If this process fails for any reason, copy the
rFactor2SharedMemoryMapPlugin64.dll from Crew Chief's plugins folder -
usually C:\Program Files (x86)\Britton IT
Ltd\CrewChiefV4\plugins\rFactor
2\Bin64\Plugins\rFactor2SharedMemoryMapPlugin64.dll to the rFactor2
'Plugins' folder (usually something like C:\Program Files
(x86)\Steam\steamapps\common\rFactor2\Bin64\Plugins). You need to enable
this plugin from the game's Options-\>Plugins UI.  
  
Note: 32 bit rFactor 2 is no longer supported.  
  
  

### rFactor2 Unofficial Features

Crew Chief supports some rF2 specific features not exposed via official
rF2 Internals API. Those features are turned off by default. To enable
those features, modify UserData\player\CustomPluginVariables.json by
setting `"EnableDirectMemoryAccess"` to "1". Plugin configuration should
look like this:  
  
`   "rFactor2SharedMemoryMapPlugin64.dll":{`  
`    " Enabled":1,`  
`    "DebugISIInternals":0,`  
`    "DebugOutputLevel":0,`  
`    "DedicatedServerMapGlobally":0,`  
`    "EnableDirectMemoryAccess":1`  
`  }`  
  
Note: first space in `" Enabled"` above is required.  
  
[See this thread for more
information](https://thecrewchief.org/showthread.php?1011-rFactor-2-Unofficial-Features)
