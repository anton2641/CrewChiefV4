# iRacing

Crew Chief is simply plug & play with iRacing, there is nothing to set
up but for the best experience Crew Chief needs at least as many cars
enabled in iRacing graphics settings (Max Cars) as the session allows,
if this value is lower then Crew Chief is more likely to give incorrect
realtime position messages. Also to make sure iRacing is sending enough
data, go to [https://members.iracing.com/membersite/account/Home.do](https://members.iracing.com/membersite/account/Home.do),
half way down the page is a setting called "Connection Type" this needs
to be as high as your internet connection allows.  
