# Pace Notes

## Recording and Playing Back Pace Notes in Crew Chief

Crew Chief provides a convenient feature for recording and playing back pace notes, which are audio messages recorded by the player as they drive around the track. When these messages are played back, they trigger at the same point on the track where they were recorded, allowing the player to receive guidance and feedback on how to handle specific sections of the track.

### Sharing Pace Notes

Pace notes can be shared between users by copying them into the correct folder structure under the Documents folder. Crew Chief expects the pace notes to be located in the following directory: `/Documents/CrewChiefV4/pace_notes/[game-name]/[car-name]/[track-name]/`. If the `car-name` part of this path is omitted, the pace notes will be available for any car class.

### Recording Pace Notes

To record new pace notes, the player needs to run the speech recognition feature in 'hold button' mode and assign a button to the "Start / stop recording pace notes" action. Then, start a practice session and press the assigned button. Crew Chief will play a voice confirmation and begin recording any voice commands the player makes. Note that the radio beep will be different when recording pace notes.

The voice recordings, along with the distance-round-track (and lap number if multi-lap pace notes are enabled), will be saved in a folder inside the CrewChiefV4 folder in the player's Documents folder, with the game name, car name, and track name. The recorded messages can be made over multiple laps or sessions, and Crew Chief will automatically add new recordings to any existing pace notes for the same game, car, and track combination.

### Playing Back Pace Notes

If there are pace notes for the game / car / track combination being used in the Documents folder, the player can start them with the voice command "start pace notes" or by assigning a button to the "Start / stop playing pace notes" action. When the pace notes are being played, each of the recorded messages will be triggered when the player reaches the corresponding point on the track where it was recorded. The pace notes will continue to be played until the player presses the button again or makes the "stop pace notes" voice command.

### Sample Pace Notes Set

For those interested in seeing pace notes in action, there is a sample pace notes set available for Raceroom's Macau circuit in the WTCC 2014 - 2017 cars. The set can be downloaded from this [link](https://d1o4ya81faadx8.cloudfront.net/ExamplePaceNotes.zip) and unzipped to the Documents folder, creating a directory structure as described above. Then, when starting a WTCC session at Macau, the player can start the pace notes playing with the "start pace notes" voice command or "Start / stop playing pace notes" button.

Note: It is important not to rename the wav files, although they can be filtered and modified as desired.