# Personalised Messages and Driver Names in Crew Chief

### Personalised Messages

The "My name" dropdown menu in the top right of the main UI allows players to select from a list of names that Crew Chief will use to address them from time to time. These names are incorporated into existing messages so that, for example, instead of just saying "box now," Crew Chief will say "box now, please Jim." There are lots of variations and they sound quite good, if players are lucky enough to have a first name that's in this list.

How often players hear these personalised messages is controlled by the "Min time between personalised messages" property. The old default value (30s) is a bit low - 60s sounds better. The personalisations come in the form of a mini-sound pack which is downloaded and managed by Crew Chief. If new personalisations are available, the "Download personalisations" button will be highlighted in green. Swapping between personalisations requires an app restart.

### Driver Names

Crew Chief has a set of pre-recorded driver names in `/sounds/driver_names`. These are used when reading messages about a particular driver (e.g. "Smith ahead is pitting"). Crew Chief also listens for driver names in voice commands (e.g. the player asks "where is Smith?").

In order to perform these operations, Crew Chief has to derive a usable driver name from the name of the driver in-game (which is called the raw name). For Project Cars, the in-game name is the driver's Steam name. In RaceRoom, the in-game name is set up when the player registers for the service (and can be changed). Typically, RaceRoom names are normal names (firstname lastname), whereas Project Cars names are player handles (combinations of letters, numbers, and other characters).

To work out what name to use, Crew Chief first looks in a file called `names.txt`, which is in `/sounds/driver_names/`. Each line in this file contains the in-game name, then a colon, then the name the game will use.

For example, if the Steam ID is "mr_belowski" and the real name is "Jim Britton," there is an entry in `names.txt` for "mr_belowski:belowski." It's important to note that this isn't the real name. When playing Project Cars, other players will see "mr_belowski" in the drivers list. If this is mapped to "Britton," when Crew Chief says "Britton is pitting now," other players will have no idea who that's referring to. So in Project Cars, "mr_belowski" becomes "belowski."
  
If there's no entry in driver_names.txt for a driver's raw driver name,
Crew Chief will do some basic parsing of the raw name to see if it can
extract something usable. It's looking for an unambiguous last name. If
the name is 2 Strings separated by a space ("Jim Britton") Crew Chief
will take the text after the space and use this. Crew Chief also knows
that text like "van ", "von ", " "de la ", "del " etc are part of a last
name. So in Race Room, Crew Chief uses "Britton" for my raw driver name
of Jim Britton, and would use "Van Dyke" for a raw name of "Dick Van
Dyke". This doesn't require an entry in names.txt. Crew Chief will trim
off numbers, replace underscores and some other characters with spaces,
remove text inside square brackets, and a few other things in other to
work out a usable name. If after doing this, it's left with a some text,
it'll use it. So a Steam ID of, say, "jimmy75\_\[ukcd\]" will be
transformed into a usable driver name of "jimmy". For Race Room and for
AI drivers this approach works well enough that most driver names don't
need an entry in names.txt. For the Steam IDs used in Project Cars, it
usually fails to get a usable driver name so PCars players will usually
need an explicit entry in names.txt.  
  
## Adding Driver Names in Crew Chief

## Deriving a usable driver name

If Crew Chief can derive a usable driver name from the in-game name of a driver, it adds it to the voice recognition engine and looks for a corresponding .wav file in the /sounds/driver_names folder. For instance, if the in-game name is "mr_belowski" and Crew Chief has mapped it to "belowski" in the names.txt file, then it will look for a file called "belowski.wav" in the driver_names folder. If it finds the file, it loads the clip and uses it when constructing messages that include a driver name. If it doesn't find the file, messages referring to that driver by name won't be played, and if it can't derive a usable driver name, Crew Chief won't be able to respond to voice requests about that driver.

## Editing the names.txt file

Users can edit the names.txt file to add mappings between in-game names and usable names. If there is already a recording in the driver_names folder that can be used for a given Steam ID or driver raw name, users can use that file. Alternatively, users can add their own driver name recordings, but these might sound a little unnatural since they'll be combined with the default voice in the messages.

Here is an example of a names.txt file:

```
Lino Carreira
Ianmiller40  
[LDGR]smokeyadam
*Falcon*
```

## Testing names with names_test.txt

Users can create a new file called names_test.txt in the root of the /sounds/ folder to test driver name mappings. They can add raw driver names, one per line, to the file. When Crew Chief starts up, it will play a few messages about each driver in that list using the standard mechanism for getting the sound files.

## Requesting new driver name recordings

If users have a bunch of names they want to add recordings for, they can ask for help. They will need to provide the raw name and the usable name in the following format:

```
Lino Carreira:Lino
Ianmiller40:Ian
[LDGR]smokeyadam:Smokey
*Falcon*:Falcon
```
