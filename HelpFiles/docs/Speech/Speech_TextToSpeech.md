# Using Text-to-Speech (TTS) Names in Crew Chief

Crew Chief includes several thousand opponent driver name recordings, but this is only a small fraction of the opponent names that players are likely to encounter. To address this issue, Crew Chief can use Windows' built-in TTS functionality to create sounds for missing driver names. While the resulting TTS names may sound wrong and immersion-breaking, they can be quite useful.

### Choosing a TTS Voice

Windows 10 includes a default voice called David (on English versions) which is the least-worst option. However, Windows 7's TTS voice is hopeless, and it is unclear what other versions sound like.

### TTS Modes

There are three basic TTS modes in Crew Chief:

1. Disabled (the default mode): no TTS is used. If Crew Chief doesn't have a driver name recording, it refers to the driver using generic terms or drops messages relating to this driver.
2. Only when necessary mode: enables TTS and the **Only use TTS when there is no alternative** option. Crew Chief will use TTS only when it has no alternative, so players will hardly ever hear TTS names. For example, if players ask "who's in front?" and Crew Chief has no driver name sound for this opponent, it will use a TTS sound instead of saying something like "I can't pronounce that."
3. Full TTS mode: Crew Chief will use TTS names rather than generic terms in exactly the same way as it would use real driver name recordings. Players will hear quite a lot of TTS names, which may sound pretty bad.

### Adjusting TTS Settings

TTS sounds tend to be much quieter than the recorded sounds and aren't filtered or compressed. To make their volume balance with other sounds, Crew Chief provides a **TTS volume boost** property, which increases the TTS sound volume by a specified factor (the default is 2 and seems to work well).

The TTS engine tends to add some silence before and lots of silence after the TTS sound. To address this issue, Crew Chief provides **Trim end of TTS sounds** and **Trim start of TTS sounds** properties. Note that in some circumstances, trimming the start can result in a wave file that can't be played. Crew Chief only allows the start to be trimmed if the resulting sound is more than one second long. If players find Crew Chief hanging when playing TTS sounds, they should reduce these trim values.