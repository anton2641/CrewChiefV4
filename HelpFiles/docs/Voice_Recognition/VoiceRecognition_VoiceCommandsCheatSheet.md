# Voice Commands Cheat Sheet

**Stripped right down to basics (omitting iRacing extras)**

#### Race status

What's my gap ahead  
What's my gap behind  
What's my position  
What's my fuel level  
How long's left  
How many laps are left  

#### Car status

Damage report  
Session status  

#### Brakes

How are my brakes  

#### Tyres

How are my tyre temps  

#### Fuel

What's my fuel usage  
How much fuel for X minutes / laps  
How much fuel to the end  

#### Pitstops

Do I have to pit  
Do I have a penalty  

#### Other drivers

Who's ahead  
Who's behind  

#### Time of day

What's the time  
Set alarm to \[hour\] \[minutes\] \[optional am/pm\]  
  