# Voice Recognition in other languages

Speech recognition is possible in other languages. You need to copy
**"%ProgramFiles(x86)%\Britton IT
Ltd\CrewChiefV4\speech_recognition_config.txt"** to
***\[user\]*/AppData/local/CrewChiefV4/** and translate the phrases, for
example  
  
**HOWS_MY_ENGINE = comment va mon moteur  
**or**  
WHATS_MY_FUEL_LEVEL = wieviel benzin habe ich noch  
**  
There are more details in speech_recognition_config.txt  
  
Translating Crew Chief's speech phrases is a *much* bigger task, see
[Customising Voice Packs](../About/About_Customising_VoicePacks.md).  
