# Voice commands

Crew Chief understands lots of command including:  
  
- `How's my \[fuel / tyre wear / body work / aero / engine / transmission / suspension / pace / delta best \]`  
- `How are my \[tyre temps / tyre temperatures / brakes / brake temps / brake temperatures / engine temps / engine temperatures\]` (gives a good / bad type response)  
- `What are my \[brake temps / tyre temps\]` (gives the actual temps)  
- `What's my \[oil temp / water temp\]` (gives the actual temps)  
- `What's my \[gap in front / gap ahead / gap behind / last lap / last lap time / lap time / position / fuel level / best lap / best lap time\]`  
- `What's the fastest lap` (reports the fastest lap in the session for the player's car class)  
- `Keep quiet / I know what I'm doing / leave me alone` (switches off messages)  
- `Keep me informed / keep me posted / keep me updated` (switches messages back on)  
- `How long's left / how many laps are left / how many laps to go`  
- `Spot / don't spot` (switches the spotter on and off - note even in
- `leave me alone` mode the spotter still operates unless you explicitly switch it off)  
- `Do I still have a penalty / do I have a penalty / have I served my penalty`  
- `Do I have to pit / do I need to pit / do I have a mandatory pit stop / do I have a mandatory stop / do I have to make a pit stop`  
- `Where's \[opponent driver last name\]`  
- `What's \[opponent driver last name\]'s last lap`  
- `What's \[opponent driver last name\]'s best lap`  
- `What's \[opponent driver last name\]'s irating`  
- `What's \[opponent driver last name\]'s license class`  
- `What's \[opponent race position\]'s last lap` (for example, `what's p 4's best lap`, or `what's position 4's last lap`)  
- `What's \[opponent race position\]'s best lap`  
- `What's \[opponent race position\]'s irating`  
- `What's \[opponent race position\]'s license class`  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s last lap`  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s best lap`  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s irating`  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s license class`  
- `What's \[the car behind / the guy behind\]'s last lap`  
- `What's \[the car behind / the guy behind\]'s best lap`  
- `What's \[the car behind / the guy behind\]'s irating`  
- `What's \[the car behind / the guy behind\]'s license class`  
- `What tyre(s) is \[opponent driver last name / opponent race position\] on`  
- `What are my sector times`  
- `What's my last sector time`  
- `Who's leading` (this one only works if you have the driver name recording for the lead car)  
- `Who's \[ahead / ahead in the race / in front / in front in the race / behind / behind in the race\]` (gives the name of the car in front / behind in the race or on the timing sheet for qual / practice. This one only works if you have the driver name recording for that driver)  
- `Who's \[ahead on track / in front on track / behind on track\]` (gives the name of the car in front / behind in on track, regardless of his race / qual position. This one only works if you have the driver name recording for that driver)  
- `Tell me the gaps / give me the gaps / tell me the deltas / give me the deltas` (switch on 'deltas' mode where the time deltas in front and behind get read out on each lap. Note that these messages will play even if you have disabled messages)  
- `Don't tell me the gaps / don't tell me the deltas / no more gaps / no more deltas` (switch off deltas mode)  
- `Repeat last message / say again` (replays the last message)  
- `What are my \[brake / tyre\] \[temperatures / temps\]`  
- `What time is it / what's the time` (reports current real-world time)  
- `What's my fuel usage / what's my fuel consumption / what's my fuel use` (reports the per-lap or per-minute average fuel consumption)  
- `What tyres am I on / what tyre am / on / what tyre type am i on` (reports the tyre name you're currently using, if available)  
- `Calculate fuel for \[X minutes / laps\] / how much fuel do I need for \[X minutes / laps\] / how much fuel for \[X minutes / laps\]` (estimates how much fuel you'll probably need for this many minutes or laps)  
- `Give me tyre pace differences / what are the tyre speeds / whats the difference between tyres / compare tyre compounds` (Raceroom only - gives lap time deltas for the best lap on each tyre type that's been used during the session, across all drivers in the same car class as the player)  
- `This is the formation lap / formation lap / rolling start`: Enable manual rolling-start mode (used by some online leagues)  
- `Standing start / no formation lap`: Disable manual rolling-start mode  
- `Where should I attack / where am I faster / where can I attack`: If Crew Chief has enough data, will report the corner name where you're gaining the most time on the guy in front 
- `Where should I defend / where am I slower / where is he faster / where will he attack`: If Crew Chief has enough data, will report the corner name where you're losing the most time to the guy behind  
- `Read corner names / corner names / tell me the corner names`: read out each corner name when you hit the mid-point of the corner, for this lap only (useful to test corner name mappings)  
- `Damage report` / `How's my car` / `Is my car ok?`: report any damage the car has sustained  
- `Car status`: report any damage the car has sustained, tyre and brake temperature status and fuel / battery status  
- `Session status` / `Race status`: report race position, gaps, time / laps left in session  
- `Full update` / `Full status` / `Update me`: combines all of the above three status reports (will produce a very verbose response)  
- `How much fuel to the end` / `how much fuel do we need`: report how many litres or gallons of fuel Crew Chief thinks you'll need to finish the race  
- `Is the car ahead in my class` / `is the car ahead my class` / `is the car ahead the same class as me` / `is the car in front in my class` /
- `is the car in front my class` / `is the car in front the same class as me` - responds yes or no  
- `Is the car behind in my class` / `is the car behind my class` / `is the car behind the same class as me` - responds yes or no  
- `What class is the car ahead` / `what class is the guy ahead` / `what class is the car in front` / `what class is the guy in front` - reports class name, or `faster` / `slower` if Crew Chief doesn't have the class name  
- `What class is the car behind` / `what class is the guy behind`- reports class name, or `faster` / `slower` if Crew Chief doesn't have the class name  
- `Time this stop` / `practice pitstop` / `time this pitstop` / `pitstop benchmark`: time the next pitstop (sector 3 + sector 1 time) to work out home much tome overall a pitstop costs - practice session only  
- `Where will I be after a stop?` / `Estimate pit exit positions` / `What will happen if I pit?`: play estimate of traffic and race positions on pit exit, if you were to pit on this lap  
- `Don't talk in the \[corners / braking zones\]` / `no messages in the \[corners / braking zones\]`: delay non-critical messages if the player is in a challenging section of the track  
- `Talk to me anywhere` / `messages at any point`: disable message delay in challenging parts of the track  
- `Set alarm to \[hour\] \[minutes\] \[optional am/pm\]` / `alarm me at\[hour\] \[minutes\] \[optional am/pm\]`: sets the alarm clock at the given time, supports both 12 and 24 hour format and multiple alarms  
- `Clear alarm clock` / `clear alarms`: clears all the alarms set  
- `Enable cut track warnings` / `play cut track warnings:warn about cuts`  
- `No cut track warnings` / `no more cut warnings:no more cut track warnings`  
- `Monitor \[opponent driver last name\]` (adds this driver to a list of drivers which Crew Chief will keep you informed about)  
- `Team mate \[opponent driver last name\]` (adds this driver to a list of drivers which Crew Chief will keep you informed about)  
- `Rival \[opponent driver last name\]` (adds this driver to a list ofdrivers which Crew Chief will keep you informed about)  
- `Cancel monitored drivers` / `stop monitoring drivers` / `stop monitoring all` (clears all monitored drivers)  
- `Stop monitoring \[opponent driver last name\]`  

### iRacing-specific commands:

- `Pitstop add \[X liters\]` (adds X amount of fuel next pitstop)  
- `Pitstop tearoff / pitstop windscreen` (enable next pitstop)  
- `Pitstop fast repair / pitstop repair` (enable fast repair next pitstop)  
- `Pitstop clear all` (clears all selected pitstop options)  
- `Pitstop clear tyres` / `pitstop don't change tyres` / `box, clear tyres` / `box, don't change tyres` (clears all tyre selections next pitstop)  
- `Pitstop clear tearoff / pitstop clear windscreen` (clears tearoff selection next pitstop)  
- `Pitstop clear fast repair` (clears fast repair selection next pitstop)  
- `Pitstop clear fuel` (clears fuel refuelling next pitstop)  
- `Pitstop change all tyres` / `box, change all tyres` (change all tyres next pitstop)  
- `Pitstop change left front tyre` (change left front tyre next pitstop)  
- `Pitstop change right front tyre` (change right front tyre next pitstop)  
- `Pitstop change left rear tyre` (change left rear tyre next pitstop)  
- `Pitstop change right rear tyre` (change right rear tyre next pitstop)  
- `Pitstop change left side tyres`(change left side tyres next pitstop)  
- `Pitstop change right side tyres`(change right side tyres next pitstop)  
- `Pitstop change tyres pressure \[ new value \]` (change all tyre pressures next pitstop)  
- `Pitstop change left front tyre pressure \[ new value \]` (change left front tyre pressure next pitstop)  
- `Pitstop change right front tyre pressure \[ new value \]` (change right front tyre pressure next pitstop)  
- `Pitstop change left rear tyre pressure \[ new value \]` (change left rear tyre pressure next pitstop)  
- `Pitstop change right rear tyre pressure \[ new value \]` (change right rear tyre pressure next pitstop)  
- `Pitstop fuel to the end` / `pitstop fuel to the end of the race` (add the fuel amount Crew Chief calculates you'll need to finish the race)    
- `What's the sof` / `what is the strength of field`  
- `How many incidents do i have` / `what's my incident count`  
- `What's the incident limit`  

### R3E-specific commands:

- `Pitstop clear tyres` / `pitstop don't change tyres` / `box, clear tyres` / `box, don't change tyres`  
- `Pitstop change all tyres` / `box, change all tyres`  
- `Pitstop change front tyres only` / `box, change front tyres only`  
- `Pitstop change rear tyres only` / `box, change rear tyres only`  
- `Pitstop next tyre compound` / `box, next tyre compound`  
- `Pitstop fix front aero only` / `box, fix front aero only`  
- `Pitstop fix rear aero only` / `box, fix rear aero only`  
- `Pitstop fix all aero` / `box, fix all aero`  
- `Pitstop don't fix aero` / `box, don't fix aero`  
- `Pitstop fix suspension` / `box, fix suspension`  
- `Pitstop don't fix suspension` / `box, don't fix suspension`  
- `Pitstop serve penalty` / `box, serve penalty`  
- `Pitstop don't serve penalty` / `box, don't serve penalty`  
- `Pitstop refuel` / `box, refuel`  
- `Pitstop don't refuel` / `box, don't refuel`  
- `Pitstop next tyre compound` / `box, next tyre compound`  
- `Pitstop hard tyres` / `box, hard tyres`  
- `Pitstop medium tyres` / `box, medium tyres`  
- `Pitstop soft tyres` / `box, soft tyres`  
- `Pitstop prime tyres` / `box, prime tyres` / `pitstop primary tyres` /
- `box, primary tyres`  
- `Pitstop option tyres:` / `box, option tyres`  
- `Pitstop alternate tyres` / `box, alternate tyres`  
- `What are the pit actions` / `what's the pitstop plan` (reports the selected actions for the next pitstop)  
- `What's \[opponent driver last name\]'s ranking` (if available)  
- `What's \[opponent driver last name\]'s reputation` (if available)  
- `What's \[opponent driver last name\]'s rating` (if available)  
- `What's \[opponent race position\]'s ranking` (if available)  
- `What's \[opponent race position\]'s reputation` (if available)  
- `What's \[opponent race position\]'s rating` (if available)  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s ranking` (if available)  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s reputation` (if available)  
- `What's \[the car in front / the guy in front / the car ahead / the guy ahead\]'s rating` (if available)  
- `What's \[the car behind / the guy behind\]'s ranking` (if available)  
- `What's \[the car behind / the guy behind\]'s reputation` (if available)  
- `What's \[the car behind / the guy behind\]'s rating` (if available)  

### rFactor 2-specific

*Select Multi-Function Display screen*  
Display  
  sectors / sector times  
  pit menu  
  tyres  
  temps / temperatures  
  race info  
  standings  
  penalties  
  
