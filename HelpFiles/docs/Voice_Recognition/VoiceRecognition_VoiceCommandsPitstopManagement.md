# Pitstop management

In all cases start the command with the word Pitstop or Box so for example "Pitstop, change all tyres" or "Box, change all tyres"

| Fuel                                           | iRacing | R3E | rFactor 2 | ACC |
|------------------------------------------------|---------|-----|-----------|-----|
| add [X litres / gallons]                       | X       |     | X         | X   |
| fill to [X litres / gallons]                   |         |     | X         |     |
| clear fuel                                     | X       |     | X         |     |
| fuel to the end / fuel to the end of the race* | X       |     | X         | X   |
| refuel                                         |         | X   |           |     |
| don't refuel                                   |         | X   |           | X   |
| fuel to the end / fuel to the end of the race  |         | X   |           | X   |
| add fuel [X litres]                            |         | X   |           | X   |

\* See also the Properties ***iracing_enable_auto_fuel_to_end_of_race***
and ***rf2****\_enable_auto_fuel_to_end_of_race*** *  
*  

| Tyres                                          | iRacing | R3E | rFactor 2 | ACC |
|------------------------------------------------|---------|-----|-----------|-----|
| change all tyres                               | X       | X   | X         | X   |
| clear tyres - don't change tyres               | X       | X   | X         | X   |
| change front tyres only                        |         | X   | X         |     |
| change rear tyres only                         |         | X   | X         |     |
| change left side tyres                         | X       |     | X         |     |
| change right side tyres                        | X       |     | X         |     |
| change left front tyre                         | X       |     | X         |     |
| change right front tyre                        | X       |     | X         |     |
| change left rear tyre                          | X       |     | X         |     |
| change right rear tyre                         | X       |     | X         |     |
| next tyre compound                             |         | X   | X         |     |
| hard tyres                                     |         | X   | X         |     |
| medium tyres                                   |         | X   | X         |     |
| soft tyres                                     |         | X   | X         |     |
| supersoft tyres                                |         |     | X         |     |
| ultrasoft tyres                                |         |     | X         |     |
| hypersoft tyres                                |         |     | X         |     |
| prime tyres                                    |         | X   | X         |     |
| primary tyres                                  |         | X   | X         |     |
| option tyres:                                  |         | X   | X         |     |
| alternate tyres                                |         | X   | X         |     |
| intermediate tyres                             |         |     | X         |     |
| dry tyres                                      |         |     |           | X   |
| wet tyres                                      |         |     | X         | X   |
| monsoon tyres                                  |         |     | X         |     |
| tyre set [ new value ]                         |         |     |           | X   |
| change tyre pressures [ new value ]            | X       |     |           | X   |
| change front tyre pressures [ new value ]      |         |     |           | X   |
| change rear tyre pressures [ new value ]       |         |     |           | X   |
| change left front tyre pressure [ new value ]  | X       |     |           | X   |
| change right front tyre pressure [ new value ] | X       |     |           | X   |
| change left rear tyre pressure [ new value ]   | X       |     |           | X   |
| change right rear tyre pressure [ new value ]  | X       |     |           | X   |

In rFactor 2 the tyre types are translated from **hard/medium/soft**
etc. to whatever is available for the vehicle.  
That requires quite a bit of magic so you may not end up with the tyres
you expect. If that happens ***Pitstop, next tyre compound*** will
select the next type of tyre or if you're adventurous you may try
editing *`<``Documents>`*`\CrewChiefV4\rF2\TyreDictionary.json`where
you'll find each tyre type and a list of words from the names of tyres
available in rF2.  
  
| Repairs              | iRacing | R3E | rFactor 2 | ACC |
|----------------------|---------|-----|-----------|-----|
| fast repair / repair | X       |     |           |     |
| clear fast repair    | X       |     |           |     |
| fix front aero only  |         | X   |           |     |
| fix rear aero only   |         | X   |           |     |
| fix all aero         |         | X   |           |     |
| don't fix aero       |         | X   |           |     |
| fix suspension       |         | X   |           |     |
| don't fix suspension |         | X   |           |     |
| fix / repair body    |         |     | X         |     |
| fix / repair all     |         |     | X         |     |
| fix / repair none    |         |     | X         |

| Penalties           | iRacing | R3E | rFactor 2 | ACC |
|---------------------|---------|-----|-----------|-----|
| serve penalty       |         | X   | X         |     |
| don't serve penalty |         | X   | X         |

| Misc                                         | iRacing | R3E | rFactor 2 | ACC |
|----------------------------------------------|---------|-----|-----------|-----|
| clear all (clears all selected options)      | X       |     | X         |     |
| tearoff / windscreen                         | X       |     |           |     |
| clear tearoff / clear windscreen             | X       |     |           |     |
| what are the pit actions? / what's the plan? |         | X   |
