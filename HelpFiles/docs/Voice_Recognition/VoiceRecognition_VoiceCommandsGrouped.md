# Voice Commands Grouped

#### Race status

What's my  
   gap in front  
   gap ahead  
   gap behind  
   last lap  
   last lap time  
   lap time  
   position  
   fuel level  
   best lap  
   best lap time  
How long's left  
   How many laps are left  
   How many laps to go  

#### Sectors/gaps

What are my sector times  
What's my last sector time  
Tell me the gaps  
   Give me the gaps  
   Tell me the deltas  
   Give me the deltas  
      *switch on 'deltas' mode where the time deltas in front and behind
get read out on each lap.*  
      *Note that these messages will play even if you have disabled
messages*  
Don't tell me the gaps  
   Don't tell me the deltas  
   No more gaps  
   No more deltas  
      *switch off deltas mode*  

#### Racecraft

Where should I attack  
   Where am I faster  
   Where can I attack  
      *If Crew Chief has enough data, will report the corner name where
you're gaining the most time on the guy in front*  
Where should I defend  
   Where am I slower  
   Where is he faster  
   Where will he attack  
      *If Crew Chief has enough data, will report the corner name where
you're losing the most time to the guy behind*  

#### Classes

Is the car ahead in my class  
   Is the car ahead my class  
   Is the car ahead the same class as me  
   Is the car in front in my class  
   Is the car in front my class  
   Is the car in front the same class as me  
      *responds yes or no*  
Is the car behind in my class  
   Is the car behind my class  
   Is the car behind the same class as me  
      *responds yes or no*  
What class is the car ahead  
   What class is the guy ahead  
   What class is the car in front  
   What class is the guy in front  
      *reports class name, or faster / slower if Crew Chief doesn't have
the class name*  
What class is the car behind  
   What class is the guy behind  
      *reports class name, or faster / slower if Crew Chief doesn't have
the class name*  

#### Car status

How's my:  
   fuel  
   tyre wear  
   body work  
   aero  
   engine  
   transmission  
   suspension  
   pace  
   delta best  
How are my:  
   tyre temps / temperatures  
   brakes  
   brake temps / temperatures  
   engine temps / temperatures  
      *gives a good / bad type response*  
Damage report  
   How's my car  
   Is my car ok?  
      *report any damage the car has sustained*  
Car status  
      *report any damage the car has sustained, tyre and brake
temperature status and fuel / battery status*  
Session status  
   Race status  
      *report race position, gaps, time*  
   laps left in session  
Full update  
   Full status  
   Update me  
      *combines all of the above three status reports*  
      *(will produce a very verbose response)*  

#### Brakes

What are my brake temperatures / temps  
     *gives the actual temps*  
How are my:  
   brakes  
   brake temps / temperatures  

#### Tyres

What are my tyre temperatures / temps  
     *gives the actual temps*  
What tyres am I on  
   What tyre am I on  
   What tyre type am I on  
      *reports the tyre name you're currently using, if available*  
How are my:  
   tyre temps / temperatures  

#### Engine

What's my  
   oil temp  
   water temp  
     *gives the actual temps*

#### Fuel

How's my fuel  
What's my fuel usage  
   What's my fuel consumption  
   What's my fuel use  
      *reports the per-lap or per-minute average fuel consumption*  
Calculate fuel for X minutes / laps  
   How much fuel do I need for X minutes / laps  
   How much fuel for X minutes / laps  
      *estimates how much fuel you'll probably need for this many
minutes or laps*  
How much fuel to the end  
   How much fuel do we need  
      *report how many litres or gallons of fuel Crew Chief thinks
you'll need to finish the race*  

#### Pitstops

Do I still have a penalty  
   Do I have a penalty  
   Have I served my penalty  
Do I have to pit  
   Do I need to pit  
   Do I have a mandatory pit stop  
   Do I have a mandatory stop  
   Do I have to make a pit stop  
  
Time this stop  
   Practice pitstop  
   Time this pitstop  
   Pitstop benchmark  
      *time the next pitstop (sector 3 + sector 1 time) to work out home
much*  
      *time overall a pitstop costs - practice session only*  
Where will I be after a stop?  
   Estimate pit exit positions  
   What will happen if I pit?  
      *play estimate of traffic and race positions on pit exit, if you
were to pit on this lap*  

#### Other drivers

Monitor \[opponent driver last name\]  
      *tells Crew Chief to report information about this driver (using
his name if available)*  
Team mate \[opponent driver last name\]  
      *tells Crew Chief to report information about this driver (using
the term 'team mate' if his name isn't available)*  
Rival \[opponent driver last name\]  
      *tells Crew Chief to report information about this driver (using
the term 'rival' if his name isn't available)*  
Cancel monitored drivers / Stop monitoring drivers / Stop monitoring
all  
      *clears all monitored drivers*  
Stop monitoring \[opponent driver last name\]  
What's the fastest lap  
      *reports the fastest lap in the session for the player's car
class*  
Who's leading  
      *this one only works if you have the driver name recording for the
lead car*  
Who's  
   ahead  
   ahead in the race  
   in front  
   in front in the race  
   behind  
   behind in the race  
      *gives the name of the car in front / behind in the race or on the
timing sheet for qual / practice.*  
      *This one only works if you have the driver name recording for
that driver*  
Who's  
   ahead on track  
   in front on track  
   behind on track  
      *gives the name of the car in front / behind in on track,
regardless of his race / qual position.*  
      *This one only works if you have the driver name recording for
that driver)*  
  
Where's \[opponent driver last name\]  
What's \[opponent driver last name\]'s last lap  
What's \[opponent driver last name\]'s best lap  
What's \[opponent race position\]'s last lap  
  
      *for example, what's p 4's best lap, or what's position 4's last
lap*  
What's \[opponent race position\]'s best lap  
What's  
   the car in front  
   the guy in front  
   the car ahead  
   the guy ahead 's last lap  
What's  
   the car in front  
   the guy in front  
   the car ahead  
   the guy ahead 's best lap  
What's  
   the car behind  
   the guy behind 's last lap  
What's  
   the car behind  
   the guy behind 's best lap  
What tyre(s) is \[opponent driver last name\] on  
What tyre(s) is \[opponent race position\] on  
What's \[opponent driver last name\]'s ranking / rating / reputation
(R3E only)  

#### Start

This is the formation lap  
   Formation lap  
   Rolling start  
      *Enable manual rolling-start mode (used by some online leagues)*  
Standing start  
   No formation lap  
      *Disable manual rolling-start mode*  

#### Verbosity

Spot  
Don't spot  
      *switches the spotter on and off - note even in leave me alone
mode the spotter*  
      *still operates unless you explicitly switch it off*  
Keep quiet  
   I know what I'm doing  
   Leave me alone  
      *switches off messages*  
Keep me informed  
   Keep me posted  
   Keep me updated  
      *switches messages back on*  
Repeat last message  
   Say again  
      *replays the last message*  
Read corner names  
   Corner names  
   Tell me the corner names  
      *read out each corner name when you hit the mid-point of the
corner,*  
      *for this lap only (useful to test corner name mappings)*  
Don't talk in the \[corners / braking zones\]  
   No messages in the corners / braking zones  
      *delay non-critical messages if the player is in a challenging
section of the track*  
Talk to me anywhere  
   Messages at any point  
      *disable message delay in challenging parts of the track*  

#### Time of day

What time is it  
   What's the time  
      *reports current real-world time*  
Set alarm to \[hour\] \[minutes\] \[optional am/pm\]  
   Alarm me at \[hour\] \[minutes\] \[optional am/pm\]  
      *sets the alarm clock at the given time, supports both 12 and 24
hour format and multiple alarms*  
Clear alarm clock  
   Clear alarms  
      *clears all the alarms set*  

## Raceroom

Give me tyre pace differences  
   What are the tyre speeds  
   Whats the difference between tyres  
   Compare tyre compounds  
      *Gives lap time deltas for the best lap on each tyre type that's
been used during the session,*  
      *across all drivers in the same car class as the player*  

## iRacing

What's \[opponent driver last name\]'s irating  
What's \[opponent driver last name\]'s license class  
What's \[opponent race position\]'s irating  
What's \[opponent race position\]'s license class  
What's the car in front  
   the guy in front  
   the car ahead  
   the guy ahead 's irating  
What's the car in front  
   the guy in front  
   the car ahead  
   the guy ahead 's license class  
What's the car behind  
   the guy behind 's irating  
What's the car behind  
   the guy behind 's license class  
  
What's the sof  
   What is the strength of field  
How many incidents do I have  
   What's my incident count  
What's the incident limit  
  
## rFactor 2

*Select Multi-Function Display screen*  
Display  
  sectors / sector times  
  pit menu  
  tyres  
  temps / temperatures  
  race info  
  standings  
  penalties  
  