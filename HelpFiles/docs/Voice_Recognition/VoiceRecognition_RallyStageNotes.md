# Rally Stage Notes

Crew Chief can act as co-driver in Dirt Rally, Dirt Rally 2 and Richard
Burns Rally. Crew Chief can read built-in stage notes for tracks which
have them (Richard Burns Rally only) but you can create your own stage
notes as you drive a stage using voice commands.  
  
### Setup

Dirt Rally and Dirt Rally 2  
  
These need to have UDP data enabled. For Dirt Rally you need to edit
Documents\My Games\DiRT
Rally\hardwaresettings\hardware_settings_config.xml. For Dirt Rally 2
it's Documents\My Games\DiRT Rally
2.0\hardwaresettings\hardware_settings_config.xml (or Documents\My
Games\DiRT Rally 2.0\hardwaresettings\hardware_settings_config_vr.xml).
Modify the udp element (under the motion_platform element) so it looks
like this:  
\<udp enabled="true" extradata="3" ip="127.0.0.1" port="20777" delay="1"
/\>  
  
Richard Burns Rally  
  
Copy the file C:\Program Files (x86)\Britton IT
Ltd\CrewChiefV4\plugins\RBR\CrewChief.dll to C:\RBR\Plugins\\
overwriting the file if it's already there (this dll was updated
recently).  
  
### Voice Recognition settings

Creating stage notes using voice commands is quite challenging for the
voice recognition system. Before you start it's a good idea to ensure
you're getting the best voice recognition accuracy possible. Optimal
settings will vary for each user but I find that it works best using the
built-in Windows speech recogniser (enable the "Prefer Windows speech
recogniser") - you need to complete all the training steps in the
Windows Speech Recognition control panel (the "Train your computer to
understand you better" option). Note that this training will only work
if you use the same microphone you'll use while you're actually
playing.  
  
There's a separate confidence threshold setting for rally voice commands
('min rally voice recognition confidence', one the Windows recogniser
and one for the Microsoft recogniser). This is significantly lower than
the regular confidence threshold because there are many permutations of
words in rally voice commands.  
  
Stage notes can be created with the 'always on' voice recognition mode
but there's a risk that the recogniser will interpret background noises
as commands so may create unwanted stage note entries. Using 'hold
button' seems to work best.  
  
### Basic Approach

There are a few options and 'gotchas' to be aware of when creating stage
notes, but if you just want to give it a try, do this:  

- set up voice recognition
- select the right game in Crew Chief (allow it to restart if it asks)
    and launch and start Crew Chief
- launch the game and mute the built-in codriver
- start a rally stage (note that 'shakedown' in Dirt / Dirt 2 isn't
    sufficient here - it doesn't allow you to drive the whole stage)
- make the voice command "start stage recce" before you start
- drive (slowly...) around the first corner or other obstacle
- as you exit the corner or pass the obstacle slow or stop and make a
    voice command (e.g. "right 3, tightens")
- Crew Chief will confirm the command back to you
- repeat for the next obstacle (leaving at least a second or so
    between commands) until you reach the end of the stage
- make the voice command "finish stage recce" \[this is essential - it
    triggers the saving process\]

If all goes well, restart the stage, drive it at warp 9, crash into a
tree and die horribly. Then marvel at how often you mixed up left and
right on your recce run.  
  
### Under the Hood

It's very helpful to have a basic grasp of what Crew Chief's doing here.
When you pass a corner or a crest or whatever and make a stage note
command, Crew Chief records the command along with distance into the
stage where your car currently is (i.e. the distance at the end of the
corner / obstacle). It uses this to estimate when the corner / obstacle
started (it assumes corners are 40 metres long, corners you describe as
'long' are 80 metres long and other obstacles are 10 metres long). This
sounds sketchy but it's not too bad. You need to have driven the corner
to understand what it'll do (if Crew Chief expected the command at the
beginning of a corner you might miss important 'tightens', 'keep in' or
other details) and it also allows for an accurate placement of the next
distance call.  
  
Crew Chief uses the distance when you first begin making the command
(unless you're using 'always on' voice recognition mode - then it has to
use the distance when you finish the command). Moving while making the
commands is fine but it makes corrections harder - if you have to
correct a command you have less time to do it before the next command
needs to be made.  
  
On playback Crew Chief uses the estimated start point (along with your
current position and car speed) to determine when it needs to play a
stage note, so these start points can be approximate.  
  
The stage notes are stored in Documents\CrewChiefV4\\game name\]\\stage
name\]\pacenotes.json, this is JSON and can be edited and shared. When
you start a stage Crew Chief will load the pacenotes.json file (in RBR
this process replaces any game-provided pace notes for that run).
Corrections in corrections.json will be applied to which ever stage
notes you're using (game-provided or user-created). If you want to go
back to the game-provided stage notes, rename or move the pacenotes.json
file.  
  
### Corrections During Stage Recce

Inevitably Crew Chief will fail to recognise some commands, misrecognise
some or (and this is more common than I'd like to admit) you'll get left
/ right mixed up and create a stage note that is so wrong it'll
definitely kill you if you don't fix it.  
During stage recce, if Crew Chief says "I didn't catch that" or
"pardon?", just wait a second then make the command again. If it keeps
failing to recognise, try phrasing the command differently (e.g. "medium
left" instead of "left 4"). If Crew Chief hears you and responds, but
the response is clearly wrong (Crew Chief misinterpreted or you were
confused and made the wrong request) make the command again but this
time start it with the word "correction, ...". Crew Chief will replace
the stage note just created with a new one.  
For example:  

- me: "left 4 crest, caution"
- app: "OK, caution ford over crest" \[four and ford do sound quite
    similar, and Crew Chief will always put 'caution' first in stage
    notes\]
- me: "correction, left 4 crest, caution"
- app: "Copy that, caution ford over crest" \[grrrr stupid app\]
- me: "correction, medium left, crest, caution"
- app: "Acknowledged, caution left 4 over crest" \[yay, finally\]

As you can see, correcting a stage note can be a bit of a faff (usually
a single correction or repeat is enough but not always). This is why
it's important to slow right down after the corner / obstacle to give
yourself time in case you need to make a correction.  
  
### Corrections While Driving a Stage

You can also correct stage notes while driving. This is a bit limited
and (apart from adjusting call positions) applies only to corners. After
a corner you can make a voice command "Correction, ..." and say what the
corner should have been. This can be a different tightness or a
different modifier (or both). Some examples:  

- app: "left 4, cut" \[drive the corner, nearly hit a tree on the apex
    and slide wide, soil pants\]
- me: "correction, 3, don't cut" \[app creates a new entry in its
    'corrections' file for this stage to replace the '4' with a '3' and
    replace 'cut' with 'dont cut'\]

You can just add a modifier too - "correction, long" or whatever. Crew
Chief tries to find the stage note for the corner you just drove and it
applies the correction. Corrections can also be used to adjust the
position of the last-played stage note. "correction, earlier" and
"correction, later" will move the last stage note's trigger point back
or forward by 50 metres. A single correction can be any combination of
these (e.g. "correction, 3, don't cut, earlier").  
  
Note that Crew Chief won't confirm corrections - there's just too much
going on to allow it to waste time reading them back to you.  
  
Another form of correction is an insert. You can make a command "insert
caution jump", for example, and Crew Chief will add a stage note.  
  
Corrections and inserts are stored in Documents\CrewChiefV4\\game
name\]\\stage name\]\corrections.json, this is JSON and can be edited.
Note that corrections can be used for user-created stage notes and (for
RBR) game-provided stage notes.  
  
### Available Commands

These are the currently supported commands, the set may grow but it's a
trade-off between a rich vocabulary and putting too much pressure on the
speech recogniser.  

- earlier calls / more notice \[call all stage notes earlier\]
- later calls \[call all stage notes later\]
- corner number first / use corner number first \[change calling
    style\]
- corner description / use corner description \[change calling style\]
- corner direction first / use corner direction first \[change calling
    style\]
- start stage reccy / start reccy / start recording pace notes / start
    recording stage notes \[starts stage recce mode\]
- finish stage reccy / finish reccy / finish recording pace notes /
    finish recording stage notes \[finishes stage recce mode and saves
    created stage notes\]
- correction \[starts a correction, recce or normal mode\]
- insert \[starts an insert, doesn't apply to recce mode\]
- earlier / sooner \[part of a correction, moves the last stage note
    so it triggers earlier, doesn't apply to recce mode\]
- later \[part of a correction, moves the last stage note so it
    triggers later, doesn't apply to recce mode\]

#### Corner types

- left
- right
- one / acute
- two / kay
- three / slow
- four / medium
- five / fast
- six / slight
- square
- hairpin
- flat

#### Corner modifiers

- cut
- don't cut
- tightens
- tightens bad
- widens / opens
- long
- maybe
- tightens then opens
- opens then tightens

#### Obstacles (note that some of the modifiers above can also be used on there own as obstacles)

- narrows
- gate / through gate
- opens
- bad camber
- bridge
- over bridge
- ford
- crest
- over crest
- jump
- big jump
- over jump
- over junction / junction / past junction / through junction
- caution
- slippy / slippery
- onto tarmac / tarmac
- onto snow / snow
- onto gravel / gravel
- onto concrete / concrete
- onto loose gravel / loose gravel
- ruts
- deep ruts
- tunnel
- left entry chicane
- right entry chicane
- care
- danger / double caution
- keep middle / keep center
- keep left
- keep right
- keep in
- keep out
- bumps / bumpy
- over rails
- tightens then opens
- opens then tightens
- rocks inside / logs inside / tree inside \[generates a 'keep out'
    note\]
- rocks outside / logs outside / tree inside \[generates a 'keep in'
    note\]
- uphill
- downhill
- brake / braking
