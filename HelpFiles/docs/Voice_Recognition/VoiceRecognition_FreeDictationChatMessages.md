# Free dictation chat messages

(experimental, iRacing, pCars2 and R3E only)  
  
Crew Chief can attempt to recognise a chat message if it's using the
Windows speech recognition engine (the one built into Windows that
requires training). This feature can be enabled with the 'Enable free
dictation chat messages' property. Crew Chief will expect the message to
start with the 'Chat free dictation start word' property value (default
is "chat"). If this feature is enabled and you make a voice command that
starts with this word, Crew Chief will attempt to recognise all the
speech input after this word. It will execute the "start chat macro"
macro (presses the chat key - 't' or 'c' depending on the game), then
it'll type the characters of the recognised speech, then execute the
"end chat macro" macro (presses the 'enter' key).  
  
For example, if you make a voice command "chat, good luck everyone" Crew
Chief will press the key to activate the in-game chat, type 'good luck
everyone' and press end. If it works. Note that this is heavily
dependent on the accuracy of the free dictation speech recogniser and is
just as likely to type 'good book ever known' or other such nonsense. I
\*strongly\* recommend going through the speech recognition training
process fully before using this feature. It's also a good idea to test
it with Notepad or another text editor running in the foreground first
so you can see what it would actually be typing when you make a
command.  
  