# Install Crew Chief

## :fontawesome-brands-windows: Windows

The CrewChiefV4 installer can be found [via the
forum](https://thecrewchief.org/forum.php). Once installed Crew Chief
will offer to update itself when there is a new version but if that
fails download the installer (which will be the latest version) and run
it.

### Getting started

After installing Crew Chief, check out the [Getting Started Guide](Getting_started/GettingStarted_General.md)

## :fontawesome-brands-android: Android

There is an **Android** version for Project Cars and Automobilista 2 on
[:fontawesome-brands-google-play: Google
Play](https://play.google.com/store/apps/details?id=com.britton_it_ltd.crewchief.full&gl=GB)
  