# Properties

Crew Chief includes a rather rudimentary preferences screen accessed by
the **Properties** button. Many many aspects of Crew Chief's behaviour
can be customised here but it's important to note that you don't *have*
to make any changes to these properties to get Crew Chief to work
correctly - the defaults are sensible settings and Crew Chief doesn't
*need* any configuration (except for swearing - see
**Speech/Swearing**).  [Speech/Swearing](../Speech/Swearing.md)
  
We've added a search capability to the window. Most of Crew Chief
options apply to all simulators, but you can also search for game
specific preferences by typing ACS, AMS, R3E, rF1, rF2 or pCars to
filter accordingly.  
