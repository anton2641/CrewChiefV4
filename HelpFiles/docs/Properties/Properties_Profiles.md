# Profiles -Properties

Crew Chief saves a set of preferences and controller bindings into
profile files saved in **My Documents\CrewChiefV4\Profiles** (.json
files).  
A profile can be loaded by activating it in the Properties window or
from the command line `-profile `*`profile file name`*.  
A new profile can be created in the Properties window, if you check
**Copy settings from current selected profile** the new profile will
contain settings from currently active profile.  
