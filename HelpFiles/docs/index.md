![Crew Chief Engineer](assets/engineer_edited_transparent.png)

# Welcome to Crew Chief

Meet your new team radio engineer and spotter, Crew Chief! Crew Chief is here to make your racing experience more immersive, exciting, and, of course, fun!

[:fontawesome-solid-download: Download Crew Chief](installing.md){ .md-button .md-button--primary }
[:fontawesome-brands-discord: Join our Discord](https://discord.com/invite/RyejNz9){ .md-button .md-button--primary }
[:fontawesome-regular-comments: Visit our Forum](https://thecrewchief.org/forum.php){ .md-button .md-button--primary }

## What is Crew Chief?

Crew Chief is a human-like, highly responsive radio engineer and spotter with a choice of voices, voice recognition, optional foul language, and thousands of driver names. And the best part? It's free open-source software! Donations are always appreciated, though. 😉

[ :fontawesome-brands-paypal: Donate to Crew Chief](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LW33XFXP4DPZE){ .md-button .md-button--primary align=right }

## 🎮 Compatible Games 🎮

Crew Chief works with a wide range of racing games, offering full support and spotter-only support for some titles:

| **Game**                 | **Support Level**                                                                          |
|:---------------------------|:---------------------------------------------------------------------------------------------|
| ARCA Sim Racing            | :fontawesome-brands-windows: Full                                                            |
| Assetto Corsa              | :fontawesome-brands-windows: Full                                                            |
| Assetto Corsa Competizione | :fontawesome-brands-windows: Some features not available                                     |
| Automobilista              | :fontawesome-brands-windows: Full                                                            |
| Automobilista 2            | :fontawesome-brands-windows: :fontawesome-brands-playstation: :fontawesome-brands-xbox: Full |
| Copa Petrobas de Marcas    | :fontawesome-brands-windows: Full                                                            |
| DiRT Rally                 | :fontawesome-brands-windows: Full                                                            |
| DiRT Rally 2               | :fontawesome-brands-windows: Full                                                            |
| F1 2018                    | :fontawesome-brands-windows: Spotter Only                                                    |
| F1 2019                    | :fontawesome-brands-windows: Spotter Only                                                    |
| F1 2020                    | :fontawesome-brands-windows: Spotter Only                                                    |
| F1 2021                    | :fontawesome-brands-windows: Spotter Only                                                    |
| Formula Truck              | :fontawesome-brands-windows: Full                                                            |
| GTR 2                      | :fontawesome-brands-windows: Full                                                            |
| iRacing                    | :fontawesome-brands-windows: Full                                                            |
| Project CARS               | :fontawesome-brands-windows: Full                                                            |
| Project CARS 2             | :fontawesome-brands-windows: :fontawesome-brands-playstation: :fontawesome-brands-xbox: Full |
| Project CARS 3             | :fontawesome-brands-windows: :fontawesome-brands-playstation: :fontawesome-brands-xbox: Full |
| RaceRoom Racing Experience | :fontawesome-brands-windows: Full                                                            |
| rFactor                    | :fontawesome-brands-windows: Full                                                            |
| rFactor 2                  | :fontawesome-brands-windows: Full                                                            |
| Richard Burns Rally        | :fontawesome-brands-windows: Full                                                            |
| Stock Car Extreme          | :fontawesome-brands-windows: Full                                                            |

Crew Chief works with Project Cars and Automobilista 2 on **:fontawesome-brands-xbox:Xbox** and **:fontawesome-brands-playstation:PS4** as long as they are connected to the same network.

## :fontawesome-brands-youtube: Videos

If you're not familiar with Crew Chief, check out these
[videos](https://thecrewchief.org/forumdisplay.php?22-Crew-Chief-videos)
