# Readme for MkDocs

MkDocs is a popular static site generator that is designed for building documentation websites. It is built in Python and uses the Markdown language to create content. This document provides instructions on how to install and use MkDocs with the `mkdocs-material` and `mkdocs-glightbox` themes.

## Install Python 3.5 or Higher

Before installing MkDocs, you need to ensure that you have Python 3.5 or higher installed on your system, and configured on your path. To check if you have Python installed, you can use the following command:

```
python --version
```

## Installing MkDocs-Material

MkDocs-Material is a popular theme for MkDocs that provides a clean and modern design for your documentation website. To install the `mkdocs-material` theme, you can use the following command:

```
pip install mkdocs-material
```

## Installing MkDocs-GLightbox

MkDocs-GLightbox is a plugin for MkDocs that allows you to add responsive and customizable lightboxes to your documentation website. To install the `mkdocs-glightbox` plugin, you can use the following command:

```
pip install mkdocs-glightbox
```

## Running and Building MkDocs

To run MkDocs and preview your documentation website locally, you can use the following command:

```
mkdocs serve
```

This will start a local server at `http://localhost:8000/`, which you can visit in your web browser to view your documentation website.

To build your documentation website for deployment, you can use the following command:

```
mkdocs build
```

This will generate a static website in the `site/` directory that you can deploy to your web server.

For more information on how to use MkDocs and its various features, please refer to the [official documentation](https://www.mkdocs.org/).

## Markdown Linting

In order to keep the Markdown files in this repository consistent, a Markdown linter is recommended. The linter used for this repository is [markdownlintlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint), which is available as a Visual Studio Code extension. This extension will automatically check your Markdown files for errors and warnings as you type.


## How mkdocs works

MkDocs is a static site generator that is designed for building documentation websites. It is built in Python and uses the Markdown language to create content. Any markdown file inside the `docs/` directory will be converted into a static HTML page when you run the `mkdocs build` command. Subdirectories inside the `docs/` directory will be converted into subdirectories inside the `site/` directory. These subdirectories will be presented as subdirectories in the navigation menu of your documentation website. The `mkdocs.yml` file is used to configure the settings for the documentation website, such as the name of the website, the navigation menu, and the theme.

### Formatting

It is important to follow the formatting guidelines for Markdown files in this repository. This will ensure that the documentation website is consistent and easy to read. The following formatting guidelines are recommended:

- Always start a document with a level 1 heading (`# Heading 1`), which will be used as the title of the page.
- Use level 2 headings (`## Heading 2`) for major sections of the document.
- Put all images or other media files in the `docs/assets/` directory.
